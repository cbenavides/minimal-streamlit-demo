# minimal-streamlit-demo

Just a simple demo testing a proposition for structuring a multi-component Streamlit app.

To install and run the example:

```
poetry install
source .venv/bin/activate
streamlit run minimal_streamlit_demo/app.py
```
