import streamlit as st

from minimal_streamlit_demo.components import Bar, Foo
from minimal_streamlit_demo.core.streamlit import StreamlitApp


# With the st.cache decorator we ensure that initialization is only done once, hence
# we could put expensive operations like loading a model used by a particular component
# If you want to test the impact, comment the decorator line and see the logs 😉
@st.cache(allow_output_mutation=True)
def create_app() -> StreamlitApp:
    return StreamlitApp([Foo(), Bar()])


app = create_app()
app()
