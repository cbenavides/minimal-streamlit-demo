import streamlit as st
from streamlit_webrtc import webrtc_streamer

from ..core.streamlit import StreamlitComponent


class Foo(StreamlitComponent):
    name = "Foo"

    def __call__(self) -> None:
        st.title("This is Foo")

        webrtc_streamer(key="sample")
