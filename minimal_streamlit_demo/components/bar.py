import streamlit as st

from ..core.streamlit import StreamlitComponent


class Bar(StreamlitComponent):
    name = "Bar"

    def __call__(self) -> None:
        st.title("This is Bar")
