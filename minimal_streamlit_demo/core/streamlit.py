from abc import ABC, abstractmethod
from typing import List

import streamlit as st
from loguru import logger


class StreamlitComponent(ABC):
    @property
    @abstractmethod
    def name(self) -> str:
        """Name used for identifying the component and for selecting it in the dropdown."""

    @abstractmethod
    def __call__(self) -> None:
        """Callback to execute at each Streamlit refresh."""


class StreamlitApp:
    def __init__(
        self,
        components: List[StreamlitComponent],
        selection_text: str = "Some selection",
    ):
        # By keeping the components in a dict, we ensure that each __call__ is fast, as
        # no comparisons have to be done
        self._components = {c.name: c for c in components}
        self._selection_text = selection_text
        logger.debug("Initialized streamlit app")

    def __call__(self) -> None:
        """Executed at each Streamlit refresh. Checks the user selection and execute
        the component's callback."""
        selection = st.sidebar.selectbox(self._selection_text, self._components.keys())
        self._components[selection]()
